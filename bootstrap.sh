#!/usr/bin/env bash

apt-get update
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password test'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password test'
apt-get install -y apache2 mysql-server-5.5 mysql-client-5.5 php5 libapache2-mod-php5 php5-mysql build-essential git php-pear php-db
pear install MDB2
/etc/init.d/apache2 restart
